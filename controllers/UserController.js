//Import the dependencies
const hashPassword = require('../utils/hash')
const bcrypt = require("bcrypt")
const _= require('lodash')
const Joi = require("joi")
const express = require('express');
const {User} =  require('../model/user.model')
//Creating a Router
var router = express.Router();

router.post('/signUp',async (req,res) =>{
    const error = validate(req.body)
    if(error) return res.send(error.details[0].message).status(400)

    let user  = await User.findOne({email:req.body.email})
    if(user) return res.send('User already registered').status(400)

    user  =  new User(_.pick(req.body, ['userName','email','password','isAdmin']))
    const hashed = await hashPassword(user.password)
    user.password = hashed
    await user.save()

    return res.send(_.pick(user,['_id','userName','email'])).status(201)
});

router.post('/logIn',async (req,res) =>{
    const error = validate(req.body)
    if(error) return res.send(error.details[0].message).status(400)

    let user  = await User.findOne({email:req.body.email})
    if(!user) return res.send('Invalid email or password').status(400)

    const validPassword = await bcrypt.compare(req.body.password,user.password)
    if(!validPassword) return res.send('invalid email or password').status(400)
    return res.send(user.generateAuthToken())
});
function validate(user){
    const Schema = {
        email: Joi.string().max(255).min(3).required().email(),
        password:Joi.string().max(255).min(3).required()
    }
    Joi.validate(user,Schema)
}
module.exports = router;