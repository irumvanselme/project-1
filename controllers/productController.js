//Import the dependencies
const adminMiddlewaare = require("../middlewares/admin")
const express = require('express');
const mongoose = require('mongoose');
const _=require("lodash")
//Creating a Router
var router = express.Router();
//Create Course model CLASS
const Product = mongoose.model('Product');
const Category = mongoose.model('Category')
//Router Controller for CREATE request
router.post('/',adminMiddlewaare,(req,res) => {
    /////const token = req.header('x-auth-token')
    //if(!token) return res.send('not authenticated..').status(401)
insertIntoMongoDB(req, res);
});
 //Router Controller for UPDATE request
router.put('/',adminMiddlewaare,(req,res) => {
    updateIntoMongoDB(req, res);
});
     
//Creating function to insert data into MongoDB
async  function insertIntoMongoDB(req,res) {
    let product = new Product(_.pick(req.body,['productName','productPrice','categoryId']))
    try{
        let check = await inCategory(req.body.categoryId)
        if(check){
            await product.save();
            return res.send(product).status(201) 
        }
        else{           
                return res.send("Category Doesn't Exist").status(400)
        }  
    }
    catch(err){
        return res.send(err).status(400)
    }
}
 
//Creating a function to update data in MongoDB
async function updateIntoMongoDB(req, res) {
    let check = await inCategory(req.body.categoryId)
    if(check){
        try{       
            Product.findOneAndUpdate({productId: req.body.productId},req.body, { new: true })
                .then(product => res.send(product))
                .catch(err => res.send(err).status(400));
        }catch(err){
            console.log("Error: "+err)
        }
    }else{
        try{
            res.send("Category Not found ..")
        }catch(err){
            console.log("Error: "+err)
        }
    }
} 
//Router to retrieve the complete list of available courses
router.get('/',(req,res) => {
    Product.find()
        .then(products => res.send(products))
        .catch(err => res.send(err).status(404));
});
   
//Router to update a course using it's ID
router.get('/:id', (req, res) => {
    Product.findById(req.params.id)
        .then(product => res.send(product))
        .catch(err => res.send(err).status(404));
});
 
//Router Controller for DELETE request
router.delete('/:id',adminMiddlewaare, (req, res) => {
    Product.findByIdAndRemove(req.params.id)
        .then(product => res.send(product))
        .catch(err => res.send(err).status(404));
});

router.get('/byCategoryId/:id',(req,res)=>{
    Product.find({categoryId: req.params.id})
        .then(category => res.send(category))
        .catch(err => res.send(err).status(404));
})
async function inCategory(id){
    let category = await Category.findOne({_id:id})
    if(category)
        return true
    else
        return false
}
module.exports = router;