//Import the dependencies
const hashPassword = require('../utils/hash')
const adminMiddleware = require("../middlewares/admin")
const mongoose = require("mongoose")
const bcrypt = require('bcrypt')
const User = mongoose.model('User1')
const Joi = require('joi')
const _= require('lodash')
const config = require('config')
const express = require('express');
//Creating a Router
var router = express.Router();
router.get('/getByAdmin/:isAdmin',(req,res)=>{
    if(req.params.isAdmin=="admin"){
        User.find({isAdmin:true})
            .then(admins => res.send(admins).status(201))
            .catch(err => res.send(err).status(400));
    }else if(req.params.isAdmin == "Non_admin"){
        User.find({isAdmin:false})
            .then(admins => res.send(admins).status(201))
            .catch(err => res.send(err).status(400));
    }
    
})
router.delete('/:id',(req,res)=>{
    User.findByIdAndDelete(req.params.id)

})
module.exports = router;