const mongoose = require('mongoose')
mongoose.connect('mongodb://localhost/mongoWorkFour', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false    
})
.then(() => console.log('connected to mongodb successfully....'))
.catch(err =>console.log('failed to connect to mongodb',err));
 
//Connecting Node and MongoDB
require("./category.model")
require("./product.model ")
require("./user.model")