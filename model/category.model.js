const mongoose = require('mongoose');
 
//Attributes of the Course object
var categorySchema = new mongoose.Schema({
    categoryName: {
        type: String,
        required:'this field is required'
    },
    categoryDescription: {
        type: String,
        required:'this field is required'
    }
});
 
mongoose.model('Category', categorySchema);